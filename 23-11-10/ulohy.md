Úlohy -- funkce (10. listopadu 2023)
====================================

Znaky
-----

### `ctype.c` ** ###

 - napište vlastní implementaci knihovny `ctype` s těmito funkcemi:
    - `isupper`
    - `islower`
    - `isalpha`
    - `isdigit`
    - `isalnum`
    - `tolower`
    - `toupper`
 - předpokládejte, že vstupní znaky jsou v kódování [ASCII](https://en.wikipedia.org/wiki/ASCII#Character_set)


Obrazce
-------

### `trojuhelnik.c` ** ###

 - viz `23-10-27`

### `trojuhelnik2.c` *** ###

 - viz `23-10-27`

Řetězce
-------

### `prvni_vyskyt_znaku.c` * ###

 - viz `23-11-03`

### `posledni_vyskyt_znaku.c` * ###

 - **vstupní argumenty**:
    1) řetězec *r*
    2) znak *z*
 - **výstup**: index posledního výskytu znaku *z* v řetězci *r* nebo `-1`, když znak v řetězci není

```shell
$ ./posledni_vyskyt_znaku abrakadabra a
10
$ ./posledni_vyskyt_znaku abrakadabra r
9
$ ./posledni_vyskyt_znaku abrakadabra k
4
$ ./posledni_vyskyt_znaku abrakadabra z
-1
```

### `palindrom.c` ** ###

 - **[palindrom](https://en.wikipedia.org/wiki/Palindrome)**: řetězec, který je zleva doprava stejný jako zprava doleva
 - **vstupní argument**: řetězec
 - **návratová hodnota**:
    - `0`, když je řetězec palindrom
    - `1`, když není

```shell
$ ./palindrom tahat; echo $?
0
$ ./palindrom palindrom; echo $?
1
$ ./palindrom 'SATOR AREPO TENET OPERA ROTAS'; echo $?
0
$ ./palindrom '99999199999'; echo $?
0
$ ./palindrom 'Kuna nese nanuk.'; echo $?
1
```

### `palindrom2.c` *** ###

 - jako předchozí úloha, ale jenom s ohledem na číslice a písmena a bez ohledu na velikost písmen:

```shell
$ ./palindrom2 'Kuna nese nanuk.'; echo $?
0
$ ./palindrom2 'A man, a plan, a canal: Panama!' echo $?
0
$ ./palindrom2 '12/21/33 12:21' echo $?
0
```

### `slovnikove_porovnavani.c` ** ###

 - **vstupní argumenty**: dva řetězce *r* a *s*
 - **výstup**:
    - `==`, když jsou oba řetězce stejné
    - `<`, když je *r* slovníkově ([asciicedně](https://en.wiktionary.org/wiki/ASCIIbetical)) před *s*
    - `>`, když je za

```shell
$ ./slovnikove_porovnavani nebe dudy
>
$ ./slovnikove_porovnavani dudy nebe
<
$ ./slovnikove_porovnavani dudy dudy
==
$ ./slovnikove_porovnavani oko okolo
<
$ ./slovnikove_porovnavani okolo oko
>
```

 - napište obdobu funkce `strcmp` deklarované v `string.h` -- ta vrací:
    - `0`, když jsou oba řetězce stejné
    - záporné číslo (ne nutně `-1`), když je první před druhým
    - kladné (ne nutně `1`), když za

### `delka_spolecneho_zacatku.c` ** ###

 - **vstupní argumenty**: dva řetězce *r* a *s*
 - **výstup**: počet začátečních znaků, které jsou stejné v *r* i *s*

```shell
$ ./delka_spolecneho_zacatku slovo slovesnost
4
$ ./delka_spolecneho_zacatku oko okolo
3
$ ./delka_spolecneho_zacatku dojmy pojmy
0
```

 - napište funkci, která vrací pro dva řetězce uvedený počet -- jak by ji šlo využít:
    - ve funkci, která zjišťuje, jestli je celý řetězec *s* začátkem *r*?
    - ve funkci pro slovníkové porovnávání dvou řetězců?

### `podretezec.c` *** ###

 - **vstupní argumenty**: dva řetězce *r* a *s*
 - **výstup**:
    - index prvního výskytu řetězce *r* jako podřetězce v řetězci *s*
    - `-1`, když se nevyskytuje

```shell
$ ./podretezec fun funeral
0
$ ./podretezec lol filologie
2
$ ./podretezec b abrakadabra
1
$ ./podretezec baba snih
-1
$ ./podretezec funeral fun
-1
$ ./podretezec 'sous-chaine' "Ceci n\'est pas une sous-chaine."
19
```

### `caesar.c` *** ###

 - viz `23-11-03`
 - napište funkci, která šifruje *jeden* znak daným posunem

### `vigenere.c` *** ###

 - **[Vigenèrova šifra](https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher)**


Čísla
-----

### `mensi_cislo2.c` * ###

 - **vstupní argumenty**: dvě celá čísla
 - **výstup**: menší z čísel

### `znamenko.c` * ###

 - **vstupní argument**: celé číslo
 - **výstup**:
    - `1`, když je *n* kladné
    - `-1`, když záporné
    - `0`, když nula

### `delitelnost.c` * ###

 - **vstupní argument**: dvě celá čísla *a* a *b*
 - **návratová hodnota programu**:
    - `2`, když je *a* nula
    - `0`, když číslo *a* (dělitel) dělí beze zbytku číslo *b* (dělence)
    - `1`, když ne
 - napište funkci, která přijímá dvě celá čísla a vrací, jestli je druhé dělitelné prvním; předpokládejte v ní, že je první nenulové

### `prestupnost.c` * ###

 - viz `23-10-20`

### `bezpecny_soucet.c` ** ###

 - hodnota typu `int` (a podobně `char`, `long int` ad.) zabírá pevně daný počet bajtů, takže má omezený rozsah -- při čtyřech bajtech od -2147 483 648 (-2^32) do 2147 483 647 (2^32 - 1)
    - konstanty pro nejmenší a největší hodnoty jednotlivých typů jsou v souboru `limits.h` -- pro `int` to je `INT_MIN` a `INT_MAX`
    - když se skutečný výsledek operace dostane mimo rozsah (*přeteče* nebo *podteče*), tak to vede k nedefinovanému chování -- obyčejně moc vysoká čísla přetečou do záporných a moc nízká podtečou do kladných, ale nemůžeme na to spoléhat

------------------------------------------------------------------------

 - **vstupní argumenty**: dvě celá čísla, která se vejdou do typu `int`
 - **výstup**:
    - hodnota konstanty `INT_MIN`, když je skutečný výsledek menší než `INT_MIN` (když podteče)
    - hodnota konstanty `INT_MAX`, když je skutečný výsledek větší než `INT_MAX` (když přeteče)
    - jinak skutečná hodnota součtu
 - napište funkci, která přijímá dvě čísla typu `int` a vrací:
    - `0`, když se jejich součet vejde do typu `int`
    - záporné číslo, když je součet menší než `INT_MIN`
    - kladné, když větší než `INT_MAX`

```shell
$ ./bezpecny_soucet 2 3
5
$ ./bezpecny_soucet 2 -3
-1
$ ./bezpecny_soucet 2000000000 2000000000
2147483647
$ ./bezpecny_soucet -2000000000 -2000000000
-2147483648
```

### `mocnina.c` ** ###

 - **vstupní argumenty**:
    1) celé číslo -- mocněnec
    2) nezáporné celé číslo -- mocnitel
 - **výstup**: mocnina

```shell
$ ./mocnina 3 0
1
$ ./mocnina 3 1
3
$ ./mocnina 3 2
9
```

### `dokonale_cislo.c` ** ###

 - **[dokonalé číslo](https://en.wikipedia.org/wiki/Perfect_number)**: přirozené číslo, které je součtem všech svých dělitelů kromě sebe sama, např. *6 = 1 + 2 + 2*
 - **vstupní argument**: přirozené číslo *n*
 - **návratová hodnota programu**:
    - `0`, když je *n* dokonalé
    - `1`, když není

```shell
$ ./dokonale_cislo 6; echo $?
0
$ ./dokonale_cislo 28; echo $?
0
$./dokonale_cislo 10; echo $?
1
$./dokonale_cislo 1; echo $?
1
```

### `horner.c` ** ###

 - **vstupní argument**: přirozené číslo *n*
 - **výstup**: *n*
 - řetězec se vstupním číslem nepřevádějte na číslo typu `int` funkcí `atoi`, ale napište ji sami
    - můžete předpokládat, že v řetězci budou jenom číslice (srv. s následující úlohou) a že se odpovídající číslo vejde do typu `int`
 - **postup** ([Hornerovo schéma](https://en.wikipedia.org/wiki/Horner%27s_method)) -- příklad pro řetězec `"713"`

| navštívené znaky | odpovídající číslo |
| --- | --- |
| `"7"` | `7` |
| `"71"` | `7 * 10 + 1 = 71` |
| `"713"` | `71 * 10 + 3 = 713` |

### `horner2.c` *** ###

 - jako předchozí úloha, ale:
    - na začátku vstupu může být libovolný počet bílých znaků
    - před číslem může být znaménko
    - za číslem může být libovolný počet nečíselných znaků
