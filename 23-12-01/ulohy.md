Úlohy (10. listopadu 2023)
==========================

Standardní vstup
----------------

 - **standardní vstup** (`stdin`) a **výstup** (`stdout`): vstup a výstup pro právě prováděný program
    - vstup obyčejně přichází z klávesnice
    - a výstup se obyčejně tiskne na obrazovku příkazového řádku
    - dají se přesměrovávat mezi soubory nebo jinými programy
 - jeden znak standardního vstupu načítá funkce `getchar` (deklarovaná tak jako další funkce pro práci se vstupem a výstupem v souboru `stdio.h`) a vrací hodnotu typu `int` (ne `char`!), a to:
    - buďto načtený znak (bajt),
    - nebo konstantu `EOF` (hodnota záleží na implementaci, např. `-1`), která označuje konec vstupu
 - příklad -- program, který přetiskuje znak po znaku standardní vstup na standardní výstup (jako unixový program `cat` bez argumentů):

```c
#include <stdbool.h>
#include <stdio.h>

int main(void) {
    while (true) {
        int znak = getchar();
        if (znak == EOF) {
            break;
        }
        putchar(znak);
    }
    return 0;
}
```

 - příkaz `break` ukončuje provádění smyčky (když je víc vnořených smyček, tak ukončuje nejvnitřnější), tzn. skáče za její konec
    - (a obdobně příkaz `continue` ukončuje provádění kola, tzn. skáče na jeho konec)
 - zatím předpokládejte, že vstup obsahuje jenom znaky z ASCII -- tzn. malá a velká písmena latinky bez diakritik, desítkové číslice, a základní bílé (mezera, tabulátor, odřádkování) a interpunkční znaky


### `delka_vstupu.c` * ###

 - žádný vstupní argumenty
 - **výstup**: počet znaků vstupu


### `cenzura.c` * ###

 - **vstupní argument**: znak `<z>`
 - **výstup**:
    - velká písmena, interpunkty a bílé znaky vstupu beze změny (funkce `isupper`, `ispunct` a `isspace` deklarované v souboru `ctype.h`)
    - místo každého jiného znaku znak `<z>`

```shell
$ echo 'Ahoj svete!' | ./cenzura _
A___ _____!
$ echo 'AAAAAAA!' | ./cenzura _
AAAAAAA!
$ echo 'Wenn ist das Nunstuck git und Slotermeyer? Ja! Beiherhund das Oder die Flipperwaldt gersput!' | ./cenzura '*'
W*** *** *** N******* *** *** S**********? J*! B********* *** O*** *** F*********** *******!
```

### `mala_bez_carek.c` * ###

 - žádný vstupní argument
 - **výstup**:
    - každé písmeno vstupu malé
    - vynechat čárky (`,`)

### `head.c` ###

 - nápodoba unixového příkazu `head`
 - **vstupní argument**: celé číslo *n*
 - **výstup**: až prvních *n* řádků vstupu

### `wc.c` *** ###

 - nápodoba unixového příkazu `wc`
 - žádný vstupní argument
 - **výstup**: `<počet řádků> <počet slov> <počet znaků>`
    - počty jsou zprava zarovnané na sedm míst -- formátovacím kódem `%7i`
    - řádek: posloupnost znaků zakončená znakem odřádkováním
       - předpokládejte, že je poslední znak vstupu znak odřádkování -- tzn. počet řádků je stejný jako počet odřádkování
    - slovo: posloupnost jiných než bílých znaků ohraničená bílými znaky (nebo začátkem vstupu)

```shell
$ echo 'Ahoj svete!' | ./wc 
1 2 12
$ echo '   Ahoj   svete!   ' | ./wc 
1 2 20
$ echo -e 'Ahoj\nsvete!'
Ahoj
svete!
$ echo -e 'Ahoj\nsvete!' | ./wc 
2 2 12
$ echo '' | ./wc 
1 0 1
$ cat text.txt 
Baby, baby, baby, oh
Like baby, baby, baby, no
Like baby, baby, baby, oh
Thought you'd always be mine, mine
$ ./wc < text.txt 
4 20 108
```

### `slova.c` *** ###

 - žádný vstupní argument
 - **výstup**: na každém řádku jedno slovo -- viz `wc.c`

```shell
$ echo 'Ahoj svete!' | ./slova 
Ahoj
svete!
$ echo '   Ahoj   svete!   ' | ./slova 
Ahoj
svete!
$ cat text.txt 
Baby, baby, baby, oh
Like baby, baby, baby, no
Like baby, baby, baby, oh
Thought you'd always be mine, mine
$ ./slova < text.txt 
Baby,
baby,
baby,
oh
Like
baby,
baby,
baby,
no
Like
baby,
baby,
baby,
oh
Thought
you'd
always
be
mine,
mine
```

### `mala_velka.c` *** ###

 - **vstupní argument**: řetězec skládající se z malých a velkých písmen -- vzorec velikostí písmen
 - **výstup**:
    - písmena vstupu s velikostmi podle vzorce
       - první písmeno (ne první znak!) vstupu má velikost prvního písmena vzorce, druhé druhého atd.
       - jakmile se dojde na konec vzorce, pokračuje se od jeho začátku
    - ostatní znaky beze změny

```shell
$ echo 'kapitalizace' | ./mala_velka mV
kApItAlIzAcE
$ echo 'To ale bylo neco jineho!' | ./mala_velka mVmmV
tO alE bYlo NeCo jInEho!
```
