Úlohy (3. listopadu 2023)
=========================


Řetězce
-------

 - **řetězec**: řada (**pole**) znaků zakončená nulovým bajtem (konstanta: `\0`)
 - délku řetězce zjišťuje funkce `strlen` deklarovaná v `string.h` (a to tak, že prochází řetězec znak po znaku, dokud nenarazí na nulový bajt)
 - k jednotlivým znakům se přistupuje **indexováním**:
    - **index**: pořadové číslo znaku v řetězci (obecně prvku v poli)
       - v C a většině jazyků je index prvního prvku `0`, druhého `1`, ... a posledního `<délka> - 1`
       - syntax: `<řetězec>[<index>]`, např. `s[0]` -- první znak řetězce `s`
 - příklad -- přetisknutí řetězce znak po znaku (ekvivalent `printf("%s", <řetězec>");`):
    - jeden znak tiskne funkce `putchar` deklarovaná v souboru `stdio.h` (anebo složitě: `printf("%c", <znak>);`)
 

```c
#include <stdio.h>
#include <string.h>

typedef char *str;

int main(int argc, str argv[]) {
    str retezec = argv[1];
    int delka = strlen(retezec);
    int i = 0;
    while (i < delka) {
        putchar(retezec[i]);
        i += 1;
    }
    return 0;
}
```


### `znak_na_indexu.c` * ###

 - **vstupní argumenty**:
    1) řetězec `<r>`
    2) celé číslo `<i>`
 - **výstup**: znak na indexu `<i>` v řetězci `<r>`
    - ale když neplatí `0 <= <i> < strlen(<r>)`, tak nic netisknout a vrátit `1`

```shell
$ ./znak_na_indexu retezec 3
e
$ ./znak_na_indexu retezec 2
t
$ ./znak_na_indexu retezec 0
r
$ ./znak_na_indexu retezec 7

$ ./znak_na_indexu retezec 7
$ echo $?
1
```

### `echo.c` * ###

 - **vstupní argument**: řetězec `<s>`
 - **výstup**: `<s>`
 - řetězec vytiskněte znak po znaku funkcí `putchar` a nepoužívejte funkci `strlen`

```shell
$ ./echo monada
monada
$ ./echo 'monoid v kategorii endofunktoru'
monoid v kategorii endofunktoru
$ ./echo ''

```

### `strlen.c` * ###

 - **vstupní argument**: řetězec
 - **výstup**: délka řetězce
    - nepoužívejte funkci `strlen`

```shell
$ ./strlen retezec
7
$ ./strlen 'ahoj svete'
10
$ ./strlen ''
0
```

### `retezec_svisle.c` * ###

 - **vstupní argument**: řetězec
 - **výstup**: každý znak řetězce na zvláštním řádku

```shell
$ ./retezec_svisle retezec
r
e
t
e
z
e
c
$ ./retezec_svisle r
r
$ ./retezec_svisle ''
```

### `pocet_vyskytu_znaku.c` * ###

 - **vstupní argumenty**: řetězec `<s>` a znak `<c>`
 - **výstup**: počet výskytů znaku `<c>` v řetězci `<s>`

```shell
$ ./pocet_vyskytu_znaku.c abrakadabra a
5
$ ./pocet_vyskytu_znaku.c abrakadabra k
1
$ ./pocet_vyskytu_znaku.c abrakadabra z
0
```

### `prvni_vyskyt_znaku.c` ** ###

 - **vstupní argumenty**:
    1) řetězec `<r>`
    2) znak `<z>`
 - **výstup**: index prvního výskytu znaku `<z>` v řetězci `<r>` nebo `-1`, když znak v řetězci není

```shell
$ ./prvni_vyskyt_znaku abrakadabra a
0
$ ./prvni_vyskyt_znaku abrakadabra r
2
$ ./prvni_vyskyt_znaku abrakadabra k
4
$ ./prvni_vyskyt_znaku abrakadabra z
-1
```

### `vyskyty_znaku.c` ** ###

 - **vstupní argumenty**: řetězec `<s>` a znak `<c>`
 - **výstup**:
    - první řádek: řetězec `<s>`
    - druhý řádek: `^` pod každým znakem řetězce `<s>` stejným jako znak `<c>`
    - třetí řádek: `<počet výskytů znaku <c>>/<délka řetězce <s>>`

```shell
./vyskyty_znaku2 abrakadabra a
abrakadabra
^  ^ ^ ^  ^
5/11
$ ./vyskyty_znaku2 abrakadabra k
abrakadabra
    ^      
1/11
$ ./vyskyty_znaku2 abrakadabra z
abrakadabra
           
0/11
```

### `hamming.c` * ###

 - **[editační vzdálenost](https://en.wikipedia.org/wiki/Edit_distance)**: míra podobnosti dvou řetězců -- počet operací, jako je nahrazení, smazání nebo přidání znaku nebo prohození sousedních znaků, které vytvoří z prvního řetězce druhý (to se může hodit třeba pro hledání návrhů na opravu špatně napsaných slov)
 - **[Hammingova vzdálenost](https://en.wikipedia.org/wiki/Hamming_distance)**: editační vzdálenost pro dva stejně dlouhé řetězce s jedinou operací -- nahrazením znaku; tzn. počet indexů s různými znaky

---------------

 - **vstupní argumenty**: dva stejně dlouhé řetězce
 - **výstup**: `<Hammingova vzdálenost řetězců>/<délka řetězců>`

```shell
$ ./hamming pocitac monitor
4/7
$ ./hamming Hamming Hamming
0/7
$ ./hamming baba snih
4/4
```

### `hamming2.c` *** ###

 - **vstupní argumenty**: dva řetězce
 - **výstup**: pět sloupců:
    - v prvním je index
    - když se znaky na daném indexu liší, tak je v druhém sloupci znak z prvního řetězce a ve čtvrtém z druhého a v pátém je znak `|`
    - jinak je společný znak ve třetím sloupci a v pátém je znak `=`

```shell
$ ./hamming2 pocitac monitor
0  p m  |
1   o   =
2  c n  |
3   i   =
4   t   =
5  a o  |
6  c r  |
$ ./hamming2 Hamming Hamming
0   H   =
1   a   =
2   m   =
3   m   =
4   i   =
5   n   =
6   g   =
$ ./hamming2 baba snih
0  b s  |
1  a n  |
2  b i  |
3  a h  |
```

 - pro různě dlouhé řetězce nic netiskněte a vraťte jedničku

### `soucet_cislic.c` ** ###

 - **vstupní argument**: nezáporné celé číslo
 - **výstup**: součet číslic

```shell
$ ./soucet_cislic 123
6
$ ./soucet_cislic 102030
6
$ ./soucet_cislic 7
7
```

 - nepřevádějte celý vstupní argument na číselný typ, sčítejte číslice znak po znaku
    - číslo odpovídající číslici dostanete takhle: `<číslice> - '0'`

### `caesar.c` *** ###

 - **[Caesarova šifra](https://en.wikipedia.org/wiki/Caesar_cipher)**: jednoduchá nahrazovací šifra -- každé písmeno se nahradí písmenem o daný počet míst v abecedě dál (a když se dojde na její konec, tak se pokračuje od začátku)

| posun / písmeno | 1 | 2 | 3 | 13 | 25 |
| --- | --- | --- | --- | --- | --- |
| **a** | b | c | d | n | z |
| **b** | c | d | e | o | a |
| **c** | d | e | f | p | b |
| **d** | e | f | g | q | c |
| **e** | f | g | h | r | d |
| **f** | g | h | i | s | e |
| **g** | h | i | j | t | f |
| **h** | i | j | k | u | g |
| **i** | j | k | l | v | h |
| **j** | k | l | m | w | i |
| **k** | l | m | n | x | j |
| **l** | m | n | o | y | k |
| **m** | n | o | p | z | l |
| **n** | o | p | q | a | m |
| **o** | p | q | r | b | n |
| **p** | q | r | s | c | o |
| **q** | r | s | t | d | p |
| **r** | s | t | u | e | q |
| **s** | t | u | v | f | r |
| **t** | u | v | w | g | s |
| **u** | v | w | x | h | t |
| **v** | w | x | y | i | u |
| **w** | x | y | z | j | v |
| **x** | y | z | a | k | w |
| **y** | z | a | b | l | x |
| **z** | a | b | c | m | y |


------------------------------------------------------------------------

 - **vstupní argumenty**:
    - řetězec `<r>`
    - celé číslo od 0 do 25 -- posun `<p>`
 - **výstup**: zašifrovaný řetězec `<r>`
    - malá písmena (latinky bez diakritik, v ASCII) jsou zašifrovaná Caesarovou šifrou s posunem `<p>`
    - ostatní znaky jsou beze změny

```shell
$ ./caesar 'Gallia omnis divisa est in partes tres.' 3
Gdoold rpqlv glylvd hvw lq sduwhv wuhv.
$ # Co je nejtezsi vec na programovani?
$ ./caesar 'Pojmenovavani promennych a chyba off by one.' 13
Pbwzrabininav cebzraalpu n pulon bss ol bar.
$ ./caesar 'Pbwzrabininav cebzraalpu n pulon bss ol bar.' 13
Pojmenovavani promennych a chyba off by one.
```

 - nepoužívejte funkci `islower` (nebo jiné funkce deklarované v `ctype.h`)
 - rozšíření:
    - posun je volitelný a má výchozí hodnotu [13](https://en.wikipedia.org/wiki/ROT13)
    - posun může být i záporný a má pak význam rozšifrovávání (text s posunem `<p>` se rozšifruje s posunem `-<p>`)
