Úvod do programování -- pole (22. prosince 2023)
================================================

 - **pole** (*array*): souvislá řada míst v paměti pro hodnoty stejného typu -- **prvků** (*elements*)
    - každý prvek má své pořadové číslo -- **index** (*index*, *subscript*)
 - deklarace pole `p` s `N` prvky typu `T`: `T p[N];`
    - např.:
       - `int cisla[3];` -- pole pro tři hodnoty typu `int`
       - `char znaky[10];` -- pole pro deset hodnot typu `char`
 - přistoupení k prvku na indexu `i`: `p[i]`
    - první prvek má index `0`, druhý `1`, poslední `N - 1`
    - `i` může být libovolný výraz (když bude výsledek platný index)
    - `p[i]` může stát kdekoliv, kde by mohla stát odpovídající obyčejná proměnná stejného typu
    - např.:
       - zapisování: `cisla[0] = 42;`
       - čtení: `printf("%i\n", cisla[0]);`
    - (tak jako u ukazatelů odráží deklarace použití: např. deklarace `int cisla[3];` naznačuje, že výraz `cisla[i]` má typ `int`)
 - příklad:

```c
#include <stdio.h>

int main(void) {
    int cisla[3];
    
    cisla[0] = 0;
    cisla[1] = 2;
    cisla[2] = 4;
    
    printf("[0] = %i\n", cisla[0]);
    printf("[1] = %i\n", cisla[1]);
    printf("[2] = %i\n", cisla[2]);
    
    return 0;
}
```

 - pro velikost pole je vhodné zavést pojmenovanou konstantu -- na to je příkaz `#define`: `#define JMENO_KONSTANTY hodnota_konstanty` (bez středníku na konci)
    - pojmenované konstanty je zvykem psát velkými písmeny
    - (je to tak jako `#include` příkaz pro předprocesor -- ten nahradí před samotným překladem všechny výskyty tokenu `JMENO_KONSTANTY` tokenem nebo řadou tokenů `hodnota_konstanty`)
    - např. `#define VELIKOST_POLE 3`

```c
#include <stdio.h>

#define POCET_CISEL 3

int main(void) {
    int cisla[POCET_CISEL];
    
    for (int i = 0; i < POCET_CISEL; i += 1) {
        cisla[i] = i * 2;
    }

    for (int i = 0; i < POCET_CISEL; i += 1) {
        printf("[%i] = %i\n", i, cisla[i]);
    }

    return 0;
}
```

 - pole se ve většině výrazů převádí na ukazatele na první prvek (*array to pointer decay*) -- tzn. `p` se chová jako `&p[0]`
    - s ukazateli na prvky pole se dá dělat **aritmetika** (*pointer arithmetic*)
       - součet ukazatele `u` a čísla `n` => ukazatel na prvek ve vzdálenosti `n` od prvku, na který ukazuje `u`
          - jednotka vzdálenosti je velikost typu prvku, ne bajt
       - rozdíl ukazatelů => vzdálenost ukazatelů
       - porovnávání operátory `<`, `<=`, `>` a `>=`
    - `p[i]` není nic víc než zkratka pro `*(p + i)` (závorky jsou tu nutné, dereference má větší prioritu než sčítání)
    - (ukazatel může ukazovat kromě libovolného prvku pole ještě na jeho konec (za poslední prvek) -- pro pole `p` s velikostí `N` je takový ukazatel `p + N` --, ale samozřejmě není možné (vede k nedefinovanému chování) ho dereferovat (tedy `*(p + N)` nebo `p[N]`))
 - na ukazatele se pole převádí i tehdy, když se předává funkci
    - parametr funkce je možné deklarovat jako `T p[]` a naznačit tak, že `p` bylo původně pole, ale znamená to totéž co `T *p`
    - obyčejně pak musí být ještě parametr pro velikost pole (ze samotného ukazatele se nedá vyčíst)
 - příklad: načtení až `MAX_POCET_CISEL` čísel ze standardního vstupu do pole a vytisknutí jejich součtu:

```c
#include <stdio.h>

#define MAX_POCET_CISEL 100

int secti(int cisla[], int pocet_cisel);   // nebo: `int *cisla`


int main(void) {
    int cisla[MAX_POCET_CISEL];
    
    int pocet_cisel;
    for (pocet_cisel = 0; pocet_cisel < MAX_POCET_CISEL; pocet_cisel += 1) {
        if (scanf("%i", &cisla[pocet_cisel]) != 1) {
            break;
        }
    }
    
    int soucet = secti(cisla, pocet_cisel);
    printf("%i\n", soucet);
    
    return 0;
}


int secti(int cisla[], int pocet_cisel) {
    int soucet = 0;
    for (int i = 0; i < pocet_cisel; i += 1) {
        soucet += cisla[i];
    }
    return soucet;
}
```

 - (to samé bez indexování, tzn. bez operátoru `[]`:)

```c
#include <stdio.h>

#define MAX_POCET_CISEL 100

int secti(int *zacatek_cisel, int *konec_cisel);


int main(void) {
    int cisla[MAX_POCET_CISEL];
    
    int *uk_cislo;
    for (uk_cislo = cisla; uk_cislo < cisla + MAX_POCET_CISEL; uk_cislo += 1) {
        if (scanf("%i", uk_cislo) != 1) {
            break;
        }
    }
    
    int soucet = secti(cisla, uk_cislo);
    printf("%i\n", soucet);
    
    return 0;
}


int secti(int *zacatek_cisel, int *konec_cisel) {
    int soucet = 0;
    for (int *uk_cislo = zacatek_cisel; uk_cislo < konec_cisel; uk_cislo += 1) {
        soucet += *uk_cislo;
    }
    return soucet;
}
```

 - **řetězec** (*string*): pole znaků (hodnot typu `char`) ukončených nulovým znakem (`\0`)
