Úlohy (27. října 2023)
======================

Obrazce
-------

### `cara.c` * ###

 - **vstupní argumenty**:
    1) celé číslo `<n>`
    2) znak `<z>`
 - **výstup**: `<n>`-krát znak `<z>`

```shell
$ ./cara 7 .
.......
$ ./cara 42 A
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
```

### `obdelnik.c` ** ###

 - **vstupní argumenty**:
    1) celé číslo -- výška obdélníka
    2) celé číslo -- šířka obdélníka
    3) znak výplně
 - **výstup**: obdélník, viz příklady

```shell
./obdelnik 3 7 O
OOOOOOO
OOOOOOO
OOOOOOO
$ ./obdelnik 4 4 -
----
----
----
----
```

### `trojuhelnik.c` ** ###

 - **vstupní argumenty**:
    1) celé číslo -- výška trojúhelníka
    2) znak výplně
 - **výstup**: trojúhelník s danou výškou, viz příklady

```shell
$ ./trojuhelnik 3 X
X
XX
XXX
$ ./trojuhelnik 5 '#'
#
##
###
####
#####
```

### `trojuhelnik2.c` *** ###

 - **vstupní argumenty**:
    1) celé číslo -- výška trojúhelníka
    2) znak výplně
    3) znak pozadí
 - **výstup**: trojúhelník s danou výškou, viz příklady

```shell
$ ./trojuhelnik2 1 X .
X
$ ./trojuhelnik2 2 X .
.X.
XXX
$ ./trojuhelnik2 3 X .
..X..
.XXX.
XXXXX
$ ./trojuhelnik2 5 O _
____O____
___OOO___
__OOOOO__
_OOOOOOO_
OOOOOOOOO
```

 - pro nekladnou výšku nic netiskněte a vraťte jedničku

```shell
$ ./trojuhelnik.c -1 X .
$ echo $?
1
```

Čísla
-----

### `posloupnost.c` * ###

 - **vstupní argumenty**:
    1) celé číslo -- první člen posloupnosti
    2) celé číslo -- první člen, který do posloupnosti už nepatří
    3) celé číslo -- rozdíl mezi jednotlivými členy posloupnosti
        - předpokládejte, že je rozdíl kladný a posloupnost rostoucí
 - **výstup**: na každém řádku jeden člen posloupnosti

```shell
$ ./posloupnost 0 3 1
0
1
2
$ ./posloupnost 1 10 2
1
3
5
7
9
```

### `posloupnost2.c` ** ###

 - jako `posloupnost.c`, ale třetí argument může být i záporný a pak je posloupnost klesající
 - když je třetí argument `0`, vraťte jedničku

```shell
$ ./posloupnost2 0 3 1
0
1
2
$ ./posloupnost2 3 0 -1
3
2
1
$ ./posloupnost2 9 0 -2
9
7
5
3
1
```

### `dvojice_cinitelu.c` ** ###

Napište program, který tiskne dvojice činitelů, na které se dá rozložit zadané číslo.

 - **vstupní argument**: přirozené číslo
 - **výstup**: dvojice činitelů vzestupně seřazené podle prvního činitele
    - první činitel je menší než druhý nebo rovný (tzn. z dvojic lišících se jenom pořadím činitelů vypište jenom jednu)

```shell
$ ./dvojice_cinitelu 12
1 12
2 6
3 4
$ ./dvojice_cinitelu 4
1 4
2 2
$ ./dvojice_cinitelu 7
1 7
```

### `collatz.c` ** ###

 - **[Collatzova posloupnost](https://en.wikipedia.org/wiki/Collatz_conjecture)**: posloupnost přirozených čísel taková, že číslo následující po čísle *n* je:
    - *n / 2*, když je *n* sudé
    - *n * 3 + 1*, když je *n* liché
 - nepotvrzená Collatzova domněnka: v každé takové posloupnosti se jednou objeví jednička

----------------------------------------

 - **vstupní argument**: přirozené číslo `<n>`
 - **výstup**: všechny členy Collatzovy posloupnosti začínající číslem `<n>` po první jedničku a jejich pořadová čísla
    - první pořadové číslo je nula
    - pořadová čísla jsou zarovnaná na tři místa -- formátovacím kódem `%3i`

```shell
$ ./collatz 4
  0) 4
  1) 2
  2) 1
$ ./collatz 12
  0) 12
  1) 6
  2) 3
  3) 10
  4) 5
  5) 16
  6) 8
  7) 4
  8) 2
  9) 1
$ ./collatz 1
  0) 1
```

### `fibonacci.h` *** ###

 - **Fibonacciova posloupnost**: posloupnost přirozených čísel začínající nulou a jedničkou, kde je každé číslo součtem předchozích dvou

----------------------------------------

 - **vstupní argument**: celé číslo `<n>`
 - **výstup**: prvních `<n>` členů Fibonacciovy posloupnosti a jejich pořadová čísla
    - ale nejvýš tolik, aby se každý vešel do typu `int`
       - nejnižší a nejvyšší hodnoty pro jednotlivé typy jsou v souboru `limits.h`, nejvyšší hodnota typu `int` je konstanta `INT_MAX`
    - první pořadové číslo je nula
    - pořadová čísla jsou zarovnaná na dvě místa (víc se do typu `int` nevejde) -- formátovacím kódem `%2i`

```shell
$ ./fibonacci 1
 0) 0
$ ./fibonacci 2
 0) 0
 1) 1
$ ./fibonacci 10
 0) 0
 1) 1
 2) 1
 3) 2
 4) 3
 5) 5
 6) 8
 7) 13
 8) 21
 9) 34
```
