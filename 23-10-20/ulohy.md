Úkoly (20. října 2023)
======================

Proměnné
--------

### `osloveni.c` * ###

 - **vstupní argument**: řetězec `<oslovení>`
 - **výstup**: `Dobry den, <oslovení>.`

```shell
$ ./osloveni Grace
Dobry den, Grace.
$ ./osloveni Linusi
Dobry den, Linusi.
```


### `miry_obdelnika.c` * ###

 - **vstupní argumenty**: dvě celá čísla -- délky stran obdélníka
 - **výstup**:
    - první řádek: `obvod = <obvod>`
    - druhý: `obsah = <obsah>`

```shell
$ ./miry_obdelnika 3 7
obvod = 20
obsah = 21
$ ./miry_obdelnika 3 3
obvod = 12
obsah = 9
```

 - pro obvod a obsah nejdřív vytvořte proměnné


### `prohozeni.c` ** ###

 - **vstupní argumenty**: dvě celá čísla `<a>` a `<b>`
 - **výstup**: 
    - první řádek: `<a> <b>`
    - druhý: `<b> <a>`

```c
#include <stdio.h>
#include <stdlib.h>

typedef char *str;

int main(int argc, str argv[]) {
    int a = atoi(argv[1]);
    int b = atoi(argv[2]);
    
    printf("%i %i\n", a, b);
    
    /*
    
    sem doplnte svuj kod
    
    */
    
    printf("%i %i\n", a, b);
    
    return 0;
}
```

 - mezi prvním a druhým voláním funkce `printf` prohoďte hodnoty proměnných `a` a `b`
    - je několik možností, jak to udělat, použijte nejjednodušší z nich -- vytvořte si ještě jednu pomocnou proměnnou:

| proměnná | hodnota na začátku | ... | hodnota na konci | 
| ---- | ---- | ---- | ---- |
| `a` | `3` | ... | `7` |
| `b` | `7` | ... | `3` |
| `pomocna` | ... | ... | ... |

 - proč by tohle nefungovalo?

```c
    // ...
    a = b;
    b = a;
    // ...
```


Větvení
-------

### `drivejsi_znak.c` * ###

 - **vstupní argumenty**: dva znaky
 - **výstup**: dřívější znak -- znak, kterému odpovídá menší číslo
    - anebo jednodušeji řečeno: když budou oba znaky velká písmena (z latinky bez diakritik) nebo oba malá, tak abecedně dřívější znak

```shell
$ ./drivejsi_znak a b
a
$ ./drivejsi_znak b a
a
$ ./drivejsi_znak a a
a
```

 - pro dřívější ze znaků zkuste nejdřív vytvořit proměnnou
 - co bude výsledek pro `A a`? a co pro `a Z`?


### `druh_znaku.c` ** ###

 - to, jestli znak patří k nějakému druhu, zjišťují funkce deklarované v souboru `ctype.h` (musí se tedy vložit příkazem `#include`), např.:

| funkce | druh znaku |
| --- | --- |
| `isalpha` | písmeno |
| `islower` | malé písmeno |
| `isupper` | velké písmeno |
| `isdigit` | číslice |
| `isalnum` | písmeno nebo číslice |

| příklad volání | výsledek |
| --- | --- |
| `isalpha('c')` | `!0` |
| `islower('c')` | `!0` |
| `isupper('c')` | `0` |
| `isupper('C')` | `!0` |
| `isdigit('7')` | `!0` |
| `isdigit('c')` | `0` |
| `isalnum('7')` | `!0` |

 - tyto funkce vracejí nenulové číslo, když znak patří do dané třídy, a nulu, když nepatří
    - není ale potřeba psát `if (isalpha('c') != 0)` (tzn. když *je* `'c'` malé písmeno) nebo `if (isalpha('c') == 0)` (*není*), stačí `if (isalpha('c'))` a `if (!isalpha('c'))` -- nula se v C obecně bere jako nepravda jakékoliv nenulové číslo jako pravda
    - (původně tu byly jako výsledky konstanty `true` a `false`, ale to byla chyba, nevrací to nutně jedničku, jak jsem si myslel)

```c
#include <ctype.h>
#include <stdio.h>

typedef char *str;

int main(int argc, str argv[]) {
    char c = argv[1][0];

    printf("Znak '%c' ");
    if (isalpha(c)) {
        printf("je");
    }
    else {
        printf("neni");
    }
    printf(" pismeno.\n");
    
    return 0;
}
```

----------

 - **vstupní argument**: znak
 - **výstup**: podle druhu znaku
    - `male pismeno`
    - `velke pismeno`
    - `cislice`
    - `jiny znak`

```shell
$ ./druh znaku A
velke pismeno
$ ./druh znaku a
male pismeno
$ ./druh znaku 0
cislice
$ ./druh znaku _
jiny znak
```

### `porovnani_delek.c` * ###

 - délku řetězce -- počet jeho znaků -- počítá funkce `strlen` deklarovaná v souboru `string.h` (musí se tedy vložit příkazem `#include`):

```c
#include <stdio.h>
#include <string.h>

typedef char *str;

int main(int argc, str argv[]) {
    str s = argv[1];
    int delka = strlen(s);
    printf("Retezec \"%s\" ma delku: %d\n", s, delka);
    return 0;
}
```

---------

 - **vstupní argumenty**: dva řetězce
 - **návratová hodnota**:
    - `0`, když je první řetězec kratší než druhý nebo stejně dlouhý
    - `1`, když je delší
 - nic netiskněte

### `porovnani_delek2.c` ** ###

 - **vstupní argumenty**: dva řetězce `<s>` a `<t>`
 - **výstup**: `|"<s>"| <vztah délek> |"<t>"|`
    - `<vztah délek>`: `<`, když je první řetězec kratší, `>`, když je delší, `==`, když jsou stejně dlouhé

```shell
$ ./porovnani_delek2 'nemit prachy' 'zazit nudu'
|"nemit prachy"| > |"zazit nudu"|
$ ./porovnani_delek2 'zazit nudu' 'nemit prachy'
|"zazit nudu"| < |"nemit prachy"|
$ ./porovnani_delek2 'zazit krachy' 'nemit prachy'
|"zazit krachy"| == |"nemit prachy"|
```

### `prestupnost.c` ** ###

Napište program, který zjišťuje, jestli je rok přestupný.

 - **vstupní argument**: přirozené číslo `<rok>`
 - **návratová hodnota**: `0`, když je `<rok>` přestupný, jinak `1`
    - nic netiskněte
 - **příklady volání programu**:

```console
$ ./prestupnost 2023; echo $?
1
$ ./prestupnost 1900; echo $?
1
$ ./prestupnost 2000; echo $?
0
```

 - místo tohohle:

```c
    bool je_prestupny;
    if (rok % 4 == 0) {
        je_prestupny = true;
    }
    else {
        je_prestupny = false;
    }
```

 - by stačilo tohle:

```c
    bool je_prestupny = rok % 4 == 0;
```

 - jenomže s přestupností je to ve skutečnosti složitější:

```c
    if (rok % 4 == 0) {
        if (rok % 100 == 0) {
            if (rok % 400 == 0) {
                je_prestupny = true;
            }
            else {
                je_prestupny = false;
            }
        else {
            je_prestupny = true;
        }
    else {
        je_prestupny = false;
    }
```

 - přepište to obdobně do jednoho výrazu -- využijte logické operace `&&` (a) a `||` (nebo)
    - operátor `&&` má větší prioritu než `||` (protože `&&` odpovídá násobení, kdežto `||` sčítání -- rozmyslete si, jak budou vypadat výsledky těchhle operací, když bude místo `true` a `false` `1` a `0`)

### `kalkulacka.c` *** ###

Napište program, který počítá základní operace s celými čísly. 

 - **vstupní argumenty**:
    1) celé číslo -- `<první operand>`
    2) znak `+`, `-`, `x` nebo `/` -- `<operátor>`
        - anebo místo `x` může být hvězdička `*` -- ale ta má v příkazovém řádku zvláštní význam, takže se pak v něm musí dát do jednoduchých uvozovek (`'*'`), aby ji bral jako obyčejný znak
    3) celé číslo -- `<druhý operand>`
 - **výstup**:
    - pro dělení: `<první operand> <operátor> <druhý operand> = <celočíselný podíl> (<zbytek>)`
    - pro ostatní operace `<první operand> <operátor> <druhý operand> = <výsledek operace>`
 - **příklady volání programu**:

```console
$ ./kalkulacka 2 + 3
2 + 3 = 5
$ ./kalkulacka 2 - 3
2 - 3 = -1
$ ./kalkulacka 2 x 3
2 x 3 = 6
$ ./kalkulacka 7 / 3
7 / 3 = 2 (1)
```

 - **základ kódu**:

```c
#include <stdio.h>
#include <stdlib.h>

typedef char *str;

int main(int argc, str argv[]) {
    int a = atoi(argv[1]);
    int b = atoi(argv[3]);
    char operator = argv[2][0];

    /*
    sem doplnte svuj kod
    */

    return 0;
}
```

 - pokuste se, aby se ve vašem kódu nic zbytečně neopakovalo -- na kolika místech byste ho museli změnit, kdyby ve výstupu třeba neměly být kolem operátoru mezery?
 - **možné rozšíření**: ošetření některých chyb uživatele:

```console
$ ./kalkulacka 3 / 0
CHYBA: deleni nulou
$ ./kalkulacka 3 2
CHYBA: spatny pocet argumentu
$ ./kalkulacka 3 ^ 2
CHYBA: neznamy operator ^
```

 - může se vám hodit: příkaz `return` okamžitě ukončuje provádění podprogramu, takže v případě podprogramu `main` i celého programu
 - jaké další chyby může uživatel udělat?
 - počet argumentů se dá zjistit z parametru `argc` funkce `main`, ale jeho hodnota je o jedna větší než počet samotných argumentů -- započítává totiž i jméno programu (v tomhle případě `./kalkulacka`) a to je pak taky první prvek parametru `argv`;
    - tímhle programem vytiskneme všechny prvky parametru `argv`:

```c
#include <stdio.h>

typedef char *str;

int main(int argc, str argv[]) {
    int i = 0;
    while (i < argc) {
        printf("argv[%d] = \"%s\"\n", i, argv[i]);
        i += 1;
    }
    
    return 0;
}
```
