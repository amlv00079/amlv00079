#include <ctype.h>
#include <stdio.h>

int main(void) {
    char c = 'A';
    char d = 'Z';
    
    printf("Vytiskněte hodnoty proměnných `c` a `d`.\n");
    
    printf("\nVytvořte proměnnou `uk_1` pro ukazatele na znak"
           " (a neinicializujte ji).\n");
    
    printf("\nPřiřaďte do `uk_1` ukazatele na `c`.\n");
    
    printf("\nVytiskněte hodnotu proměnné `uk_1`.\n");
    
    printf("\nVytiskněte hodnotu proměnné, na kterou `uk_1` ukazuje.\n");
    
    printf("\nVytvořte proměnnou `uk_2` pro ukazatele na znak"
           " a inicializujte ji ukazatelem na `d`.\n");
    
    printf("\nVytiskněte hodnotu proměnné `uk_2`.\n");
    
    printf("\nVytiskněte hodnotu proměnné, na kterou `uk_2` ukazuje.\n");
    
    printf("\nZapište do proměnné, na kterou `uk_2` ukazuje, hodnotu `'9'`.\n");
    
    printf("\nVytiskněte hodnotu proměnné `d`.\n");
    
    printf("\nZapište do proměnné `c` hodnotu `'0'.\n");
    
    printf("\nVytiskněte hodnotu proměnné, na kterou ukazuje `uk_1`.\n");
    
    printf("\nZapište do proměnné, na kterou `uk_1` ukazuje,"
           " hodnotu proměnné, na kterou ukazuje `uk_2`.\n");
    
    printf("\nVytiskněte hodnoty proměnných `c` a `d`.\n");
    
    printf("\nZapište do proměnné `d` hodnotu `'?'`.\n");
    
    printf("\nVytiskněte hodnotu proměnné `c`.\n");
    
    printf("\nZapište do proměnné `uk_1` ukazatele na proměnnou,"
           " na kterou ukazuje `uk_2`.\n");
    
    printf("\nVytiskněte hodnoty proměnných `uk_1` a `uk_2`.\n");
    
    printf("\nZapište do proměnné, na kterou ukazuje `uk_1`,"
           " hodnotu `'*'`.\n");
    
    printf("\nVytiskněte hodnotu proměnné, na kterou ukazuje `uk_2`.\n");
    
    printf("\nZapište do proměnné `uk_2` nulového ukazatele.\n");

    printf("\nVytvořte proměnnou `uk_3` pro ukazatele na znak"
            "a inicializujte ji ukazatelem na proměnnou,"
            " na kterou ukazuje `uk_1`.\n");
    
    printf("\nVytiskněte hodnotu proměnné `uk_3`.\n");
    
    printf("\nPřičtěte k hodnotě, na kterou `uk_3` ukazuje, jedničku.\n");
    
    printf("\nJestli ukazuje `uk_3` na proměnnou obsahující interpunkt,"
           " vytiskněte `ANO`.\n");
    
    printf("\nZapište do proměnné `uk_3` ukazatele na proměnnou,"
           " na kterou ukazuje `uk_2`.\n");
    
    printf("\nVytiskněte hodnotu proměnné `uk_3`.\n");
    
    printf("\nVytiskněte hodnotu na místě, na které ukazuje `uk_3`.\n");
    
    return 0;
}
