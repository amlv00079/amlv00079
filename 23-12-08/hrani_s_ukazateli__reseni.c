#include <ctype.h>
#include <stdio.h>


int main(void) {
    char c = 'A';
    char d = 'Z';
    
    printf("Vytiskněte hodnoty proměnných `c` a `d`.\n");
    printf("%c, %c\n", c, d);
    
    printf("\nVytvořte proměnnou `uk_1` pro ukazatele na znak"
           " (a neinicializujte ji).\n");
    char *uk_1;
    
    printf("\nPřiřaďte do `uk_1` ukazatele na `c`.\n");
    uk_1 = &c;
    
    printf("\nVytiskněte hodnotu proměnné `uk_1`.\n");
    printf("%p\n", uk_1);
    
    printf("\nVytiskněte hodnotu proměnné, na kterou `uk_1` ukazuje.\n");
    printf("%c\n", *uk_1);
    
    printf("\nVytvořte proměnnou `uk_2` pro ukazatele na znak"
           " a inicializujte ji ukazatelem na `d`.\n");
    char *uk_2 = &d;
    
    printf("\nVytiskněte hodnotu proměnné `uk_2`.\n");
    printf("%p\n", uk_2);
    
    printf("\nVytiskněte hodnotu proměnné, na kterou `uk_2` ukazuje.\n");
    printf("%c\n", *uk_2);
    
    printf("\nZapište do proměnné, na kterou `uk_2` ukazuje, hodnotu `'9'`.\n");
    *uk_2 = '9';
    
    printf("\nVytiskněte hodnotu proměnné `d`.\n");
    printf("%c\n", d);
    
    printf("\nZapište do proměnné `c` hodnotu `'0'.\n");
    c = '0';
    
    printf("\nVytiskněte hodnotu proměnné, na kterou ukazuje `uk_1`.\n");
    printf("%c\n", *uk_1);
    
    printf("\nZapište do proměnné, na kterou `uk_1` ukazuje,"
           " hodnotu proměnné, na kterou ukazuje `uk_2`.\n");
    *uk_1 = *uk_2;
    
    printf("\nVytiskněte hodnoty proměnných `c` a `d`.\n");
    printf("%c, %c\n", c, d);
    
    printf("\nZapište do proměnné `d` hodnotu `'?'`.\n");
    d = '?';
    
    printf("\nVytiskněte hodnotu proměnné `c`.\n");
    printf("%c\n", c);
    
    printf("\nZapište do proměnné `uk_1` ukazatele na proměnnou,"
           " na kterou ukazuje `uk_2`.\n");
    uk_1 = uk_2;
    
    printf("\nVytiskněte hodnoty proměnných `uk_1` a `uk_2`.\n");
    printf("%p, %p\n", uk_1, uk_2);
    
    printf("\nZapište do proměnné, na kterou ukazuje `uk_1`,"
           " hodnotu `'*'`.\n");
    *uk_1 = '*';
    
    printf("\nVytiskněte hodnotu proměnné, na kterou ukazuje `uk_2`.\n");
    printf("%c\n", *uk_2);
    
    printf("\nZapište do proměnné `uk_2` nulového ukazatele.\n");
    uk_2 = NULL;

    printf("\nVytvořte proměnnou `uk_3` pro ukazatele na znak"
            "a inicializujte ji ukazatelem na proměnnou,"
            " na kterou ukazuje `uk_1`.\n");
    char *uk_3 = uk_1;
    
    printf("\nVytiskněte hodnotu proměnné `uk_3`.\n");
    printf("%p\n", uk_3);
    
    printf("\nPřičtěte k hodnotě, na kterou `uk_3` ukazuje, jedničku.\n");
    *uk_3 += 1;
    
    printf("\nJestli ukazuje `uk_3` na proměnnou obsahující interpunkt,"
           " vytiskněte `ANO`.\n");
    if (ispunct(*uk_3)) {
        printf("ANO\n");
    } 
    
    printf("\nZapište do proměnné `uk_3` ukazatele na proměnnou,"
           " na kterou ukazuje `uk_2`.\n");
    uk_3 = uk_2;
    
    printf("\nVytiskněte hodnotu proměnné `uk_3`.\n");
    printf("%p\n", uk_3);
    
    printf("\nVytiskněte hodnotu na místě, na které ukazuje `uk_3`.\n");
    // printf("%c\n", *uk_3);
    /* `uk_3` je nulový ukazatel, tzn. nikam neukazuje a jeho
     * dereference vede k nedefinovanému chování -- když máme štěstí,
     * tak na tom místě program spadne
     */
    
    return 0;
}
