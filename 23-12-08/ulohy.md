Úvod do programování -- ukazatelé (8. prosince 2023)
====================================================

 - **proměnná** (*variable*): pojmenované místo v paměti
    - je (začíná) na nějaké adrese
 - **ukazatel** (*pointer*): odkaz na proměnnou -- nebo obecně libovolné místo v paměti
    - v nejjednodušší podobě jeho adresa
    - přistupovat k proměnné[^1] jménem, tzn. přímo, je možné, jenom když se zrovna provádí volání, při kterém vznikla -- ale přes ukazatele, tzn. nepřímo, je možné k ní přistupovat po celou dobu, co existuje -- i při jiném volání
 - deklarace proměnné `u` pro ukazatele na proměnnou typu `T`: `T *u;`
    - např.:
       - `int *ukazatel_na_int;`
       - `char *ukazatel_na_char;`
       - `int **ukazatel_na_ukazatele_na_int;`
       - `int *funkce_vracejici_ukazatele_na_int(...);`
 - vytvoření ukazatele na proměnnou `x` -- operátorem **reference** `&`: `&x`
    - např.:
       - `int a; int *uk_a = &a;`
       - `char c; char *uk_c; uk_c = &c;`
 - přistoupení k proměnné, na kterou ukazatel `u` ukazuje -- operátorem **dereference** `*`: `*u`
    - `*u` může stát kdekoliv, kde by mohla stát odpovídající proměnná
    - např.:
       - čtení: `printf("%i\n", *uk_a);`, `char d = *uk_c;`
       - zapisování: `*uk_a = 7;`, `*uk_c = 'A';`
    - (deklarace v C odrážejí použití: např. deklarace `int *uk;` naznačuje, že běžné použití ukazatele, tedy výraz `*uk`, má typ `int`)
 - porovnávání operátory `==` a `!=`
    - (zjišťují, jestli se rovnají ukazatelé, ne hodnoty proměnných, na které ukazují)
 - nulový ukazatel: ukazatel, který nikam neukazuje
    - konstanta `NULL`, definovaná v `stddef.h`, ale obsažená i v `stdio.h` a jinde
    - dereferování vede k nedefinovanému chování
 - (ukazatelé mají ve funkcích jako `printf` formátovací kód `%p`)

------------------------------------------------------------------------

 - odkazy (na zdroje v sylabu)
    - *Úvod do programování*, kapitola *Ukazatele*: <https://mrlvsb.github.io/upr-skripta/c/prace_s_pameti/ukazatele.html>
       - můžete přeskočit oddíly *Aritmetika s ukazateli*, *Konstantní ukazatele* a *Definice více ukazatelů najednou*
    - *CS50*, video *Pointers*: <https://www.youtube.com/watch?v=XISnO2YhnsY>
    - v obojím píšou místo `T *ukazatel;` `T* ukazatel;`, aby deklarace ukazatelů vypadaly logičtěji -- jako by `*` patřila k typu `T` a dohromady znamenaly typ ukazatel na proměnnou typu `T` --, ale to je jenom přelud

------------------------------------------------------------------------

 - příklad: program s funkcí pro prohození hodnot dvou proměnných

```c
#include <stdio.h>

void prohod(int *uk_a, int *uk_b);

int main(void) {
    int x = 3;
    int y = 7;
    printf("x = %i, y = %i", x, y);

    prohod(&x, &y);
    printf("x = %i, y = %i", x, y);

    return 0;
}


void prohod(int *uk_a, int *uk_b) {
    int pomocna = *uk_a;
    *uk_a = *uk_b;
    *uk_b = pomocna;
}
```


[^1]: přesně řečeno k lokální automatické proměnné -- tzn. proměnné definované ve funkci (proto *lokální*), která vzniká při volání na místě definice a zaniká na konci bloku, kde byla definována, nejpozději tedy na konci volání (proto *automatické* -- o vytvoření i zničení se stará překladač) -- takové jsou všechny proměnné, se kterými zatím pracujeme
[^2]: (i když ta jedna věc může být i několik věcí seskupených do jedné, ale k tomu jindy)


### `hrani_s_ukazateli.c` *** ###

 * viz soubor `hrani_s_ukazateli.c`
 * kreslete si, jak se postupně mění obsah jednotlivých proměnných (ukazatele znázorňujte šipkou na proměnnou, na kterou ukazují)
 * (hvězdičky jsou za délku, ne složitost jednotlivých kroků)


### `soucet.c` * ###

 * **vstupní argumenty**: dvě celá čísla
 * **výstup**: součet
 * vytvořte a použijte funkci, která:
    - přijímá dvě čísla *a* a *b* typu `int` a jednoho ukazatele *u* na číslo typu `int`
    - a do místa, kam *u* ukazuje, zapisuje součet *a* a *b*


### `soucet_2.c` * ###

 * vytvořte a použijte funkci, která:
    - přijímá číslo *a* typu `int` a ukazatele *u* na číslo typu `int`
    - a do místa, kam *u* ukazuje, přičítá *a*


### `bezpecny_soucet.c` ** ###

 - vytvořte a použijte funkci, která:
    - přijímá dvě čísla *a* a *b* typu `int` a jednoho ukazatele *u* na číslo typu `int`,
    - do místa, kam *u* ukazuje, zapisuje
       - `INT_MAX`, když součet *a* a *b* přeteče,
       - `INT_MIN`, když podteče,
       - jinak součet,
    - a vrací po řadě `1`, `-1` nebo `0`


### `mensi_vetsi.c` * ###

 - **vstupní argumenty**: dvě celá čísla
 - **výstup**: na prvním řádku menší z čísel, na druhém větší
 - vytvořte a použijte funkci, která:
    - přijímá dvě čísla *a* a *b* typu `int` a dva ukazatele *u* a *v* na čísla typu `int`
    - a do *u* zapisuje menší z čísel *a* a *b* a do *v* větší
 - co se stane, když do *u* a *v* dáme ukazatele na *a* a *b*?


### `mensi_vetsi_2.c` * ###

 - vytvořte a použijte funkci, která:
    - přijímá dva ukazatele *u* a *v* na čísla typu `int`
    - a do *u* zapisuje menší z čísel, na která *u* a *v* ukazují, a do *v* větší


### `mensi_vetsi_3.c` ** ###

 - **výstup**: jenom menší z čísel
 - vytvořte a použijte funkci, která:
    - přijímá dva ukazatele *u* a *v* na čísla typu `int`
    - a vrací toho ukazatele, který ukazuje na menší z čísel


### `mensi_vetsi_4.c` *** ###

 - (**výstup**: na prvním řádku menší z čísel, na druhém větší)
 - vytvořte a použijte funkci, která:
    - přijímá dva ukazatele *u* a *v* na čísla typu `int` a dva ukazatele *x* a *y* na ukazatele na `int` 
    - do proměnné, na kterou ukazuje *x*, zapisuje toho ukazatele z *u* a *v*, který ukazuje na menší z čísel, do proměnné, na kterou ukazuje *y*, toho, který na větší


### `mensi_vetsi_5.c` *** ###

 - (**výstup**: na prvním řádku menší z čísel, na druhém větší)
 - vytvořte a použijte funkci, která:
    - přijímá dva ukazatele *u* a *v* na ukazatele na `int`
    - do proměnné, na kterou ukazuje *u*, zapisuje toho ukazatele z *u* a *v*, který ukazuje na menší z čísel, do proměnné, na kterou ukazuje *v*, toho, který na větší


### `cas.c` ** ###

 - **vstupní argument**: celé číslo -- počet vteřin
 - **výstup**: `<počet_hodin> h <počet_minut> m <počet vteřin> s`
 - vytvořte a použijte funkci, která:
    - přijímá jedno číslo *a* typu `int` a tři ukazatele *u*, *v*, *w* na číslo typu `int`
    - a zapisuje do míst, na která ukazují *u*, *v* a *w* po řadě odpovídající počet hodin, minut a vteřin


### `cat.c` * ###

 - **vstup**: celý standardní vstup
 - **výstup**: obsah standardního vstupu beze změny
 - vytvořte a použijte funkci, která:
    - přijímá ukazatele *u* na znak typu `char`,
    - pokouší se načíst jeden znak (bajt) ze standardního vstupu funkcí `getchar`,
    - když se to povede, tak ho zapisuje na místo, kam ukazuje *u*, a vrací `true`,
    - jinak vrací `false` (a místo, kam *u* ukazuje, nechává beze změny)
 - některé hodnoty širších typů (např. `int` -- obyčejně čtyři bajty) se nevejdou do užšího místa (`char` -- jeden bajt) a pak se z nich ztrácí informace
    - když víme, že se širší hodnota vejde (nebo když nám nevadí, co se s ní stane, když se nevejde), tak to můžeme naznačit výslovným přetypováním: `(<typ>) <výraz>`
       - např. `int i = 65; char c = (char) i;`


Práce s proudem čísel
---------------------

 - v úlohách v tomto oddíle jsou ve vstupu celá čísla (co se vejdou do typu `int`) oddělená bílými znaky
 - funkce `scanf`: protějšek funkce `printf` pro standardní *vstup*
    - načítá hodnoty odpovídající formátovacím kódům do odpovídajících proměnných předaných jako ukazatelé
    - vrací počet úspěšně načtených hodnot nebo `EOF`, když vstup skončil
    - např. `scanf("%i", &i);`
       - (přeskáče začáteční bílé znaky a) načte číslo typu `int` do proměnné `i`
       - a vrací
          - `EOF`, když skončí vstup
          - `0`, když ve vstupu není číslo
          - `1`, když je

------------------------------------------------------------------------
    
 - třeba takhle by mohl vypadat program, co zjišťuje součet zadaných čísel:

```c
#include <stdbool.h>
#include <stdio.h>

int main(void) {
    int soucet = 0;
    while (true) {
        int cislo;
        if (scanf("%i", &cislo) != 1) {
            break;
        }
        soucet += cislo;
    }
    printf("%i\n", soucet);
    return 0;
}
```

### `nejmensi_cislo.c` * ###

 - **výstup**: nejmenší číslo ze vstupu
 - můžete předpokládat, že ve vstupu bude aspoň jedno číslo

### `prubezny_soucet.c` * ###

 - **výstup**: průběžný součet -- na každém řádku součet dosavadních čísel ze vstupu

### `hadani_cisla.c` ** ###

 - tento program vygeneruje náhodné číslo ze zadaného rozsahu a uživatel se ho pak snaží uhodnout
    - když se uživatel nestrefí, tak program řekne, jestli je hádané číslo menší, nebo větší
 - **vstupní argumenty**: dvě celá čísla (první menší než druhé) -- nejmenší a největší možné náhodné číslo
 - postup pro získání náhodného čísla:
    - `#include <stdlib.h>`
    - nastavení generátoru náhodných čísel: `srand(time(NULL));` (`time(NULL)` vrací současný čas)
       - (i kdyby se mělo generovat náhodných čísel víc, tak tohle dělejte jenom jednou)
    - vygenerování náhodného čísla: `rand()`
       - číslo je v rozsahu `0` až `RAND_MAX` (nejmíň `2^15 - 1`, tzn. nejvyšší možná hodnota dvoubajtového znaménkového celého čísla -- ale na dnešních počítačích to bude spíš `2^31 - 1`)
       - jak z takového čísla dostat číslo v rozsahu zadaném vstupními argumenty?
    - takhle získaná čísla nejsou ve skutečnosti náhodná, jsou to jenom členy nahodile vypadající posloupnosti
       - `s` v `srand` je *semínko* (*seed*), ze kterého se nějak vytváří první člen (a z toho druhý atd.); ze stejného semínka vznikne vždycky stejná posloupnost

```c
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main(void) {
    srand(time(NULL));
    int nahodne_cislo = rand();
    return 0;
}
```

 - jak číslo uhodnout co nejrychleji?

### `nejmensi_a_nejvetsi_cislo.c` *** ###

 - **výstup**: dva řádky:
    1) nejmenší číslo ze vstupu
    2) největší číslo ze vstupu
 - postup: čtěte čísla po dvou, menší porovnávejte s dosavadním nejmenším, větší s dosavadním největším
