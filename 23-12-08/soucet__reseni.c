#include <stdio.h>
#include <stdlib.h>

typedef char *str;

void secti(int a, int b, int *uk_soucet);


int main(int argc, str argv[]) {
    int x = atoi(argv[1]);
    int y = atoi(argv[2]);

    int soucet;
    secti(x, y, &soucet);
    
    printf("%i\n", soucet);
    
    return 0;
}


void secti(int a, int b, int *uk_soucet) {
    *uk_soucet = a + b;
}
