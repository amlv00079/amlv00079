#include <stdio.h>
#include <stdlib.h>

typedef char *str;

void urci_mensi_a_vetsi_cislo(int a, int b, int *uk_mensi, int *uk_vetsi);


int main(int argc, str argv[]) {
    int x = atoi(argv[1]);
    int y = atoi(argv[2]);

    int mensi;
    int vetsi;
    urci_mensi_a_vetsi_cislo(x, y, &mensi, &vetsi);
    
    printf("%i\n%i\n", mensi, vetsi);
    
    return 0;
}


void urci_mensi_a_vetsi_cislo(int a, int b, int *uk_mensi, int *uk_vetsi) {
    if (a <= b) {
        *uk_mensi = a;
        *uk_vetsi = b;
    }
    else {
        *uk_mensi = b;
        *uk_vetsi = a;
    }
}
